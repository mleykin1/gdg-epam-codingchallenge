package com.gdgnn.gdgnnapp.base.mvp;

public interface IPresenter {
    void attachView(IView view);
    void detachView();
    void onViewCreated();
}
