package com.gdgnn.gdgnnapp.base.baseFragment;

import com.gdgnn.gdgnnapp.base.mvp.IPresenter;
import com.gdgnn.gdgnnapp.base.mvp.IView;

public interface BaseFragmentContract {
    interface View extends IView {}
    interface Presenter extends IPresenter {}
}
