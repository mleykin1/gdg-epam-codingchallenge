package com.gdgnn.gdgnnapp.base.baseFragment;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;
import com.gdgnn.gdgnnapp.base.mvp.IView;

import org.jetbrains.annotations.NotNull;

public abstract class BasePresenter<V extends BaseFragmentContract.View> implements BaseFragmentContract.Presenter {

    private V view;

    protected BasePresenter(V view) {
        this.view = view;
    }

    @Override
    public void attachView(IView view) {
        this.view = (V) view;
    }

    @Override
    public void detachView() {
        this.view = null;
    }

    @Override
    public void onViewCreated() {

    }
}
