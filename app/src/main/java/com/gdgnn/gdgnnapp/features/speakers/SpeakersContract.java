package com.gdgnn.gdgnnapp.features.speakers;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

interface SpeakersContract {
    interface View extends BaseFragmentContract.View {
        void setText(String text);
    }

    interface Presenter extends BaseFragmentContract.Presenter { }
}
