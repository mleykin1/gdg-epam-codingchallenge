package com.gdgnn.gdgnnapp.features.home;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

public interface HomeContract {
    interface View extends BaseFragmentContract.View { }
    interface Presenter extends BaseFragmentContract.Presenter { }
}
