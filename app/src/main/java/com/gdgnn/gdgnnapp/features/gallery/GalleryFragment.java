package com.gdgnn.gdgnnapp.features.gallery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.features.photoDetail.PhotoDetailDialogFragment;

public class GalleryFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_gallery, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // this is for detailed photo, on gallery item click launch this with selected url - example is below
        PhotoDetailDialogFragment.newInstance("https://lh3.googleusercontent.com/dfKAmvzZS-DgJ132rHKKxSeTymzFdYWOTLdVZOb3pB-AEipAQAgv-SJGo6QUKgx6uZfa9iQWtJib1EmeNqRz9Wjss1PflR0kdDkGDEET3mpIPo0KcXqqAV6NWYddvhuUpS4Of7EnknFPKDvbihM4fDyyd7Cu2OXYBKqzfVj11M7yGKGw0wVbr1hYEVaQZhZ3JWWgXgctlwxI_Fl0BQAx33n9RqHny-Em0VpvMSe9zVuvgX1zekPEGAQ707MmqTLLDhPi0CFThZFhal9drg9eWwl4ba2BM1esdWJwGTJz_q5o018rrY5EFli4bdHgnJ0KbXe9ZYZFIn5fyGk7-_3TbakKt-ONNsA8PhEZS6kXkTl9DGLcjKXC3xZTKBV7e7rFGX3YqxFIie2bxXXAQGxn4c9l5U1GnGgejF7lVkkG0v7g8lTZ1zTmYREzc5ziY3eolA5DlgIi07kwt8OmUwfr2RiXW3jUenUXumHrLS6aIXrQ1o2SUvMAmsyzurJKN0Oktti8ez6h97P_y9Ka1APntmiv619WGrjmQBd1icE7ivm8kFt2YLKRgfxoJd3PqUNP1S8cu1xo9-HNAOVXLiMU8bLHDaMBJ0uT9lxHkjs0ynYWwtBcyhwVuXYEY6Z0o1Bg1jBjZk3G4dboVw54K-4bAJRv1zTX3QeZjcV_GUbnr6OdS7ww244PEoCqCLBoGWpiQ9Z1knTVTVkujiIxCKD4I6NMMhUkgQkimyexfTuevJeCF9jTFg=w1454-h969-no")
                .show(getFragmentManager(), null);
    }
}
