package com.gdgnn.gdgnnapp.features.share;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

public class ShareContract {
    interface View extends BaseFragmentContract.View {
        void setText(String text);
    }
    interface Presenter extends BaseFragmentContract.Presenter { }
}
