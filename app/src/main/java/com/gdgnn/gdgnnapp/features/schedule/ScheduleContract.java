package com.gdgnn.gdgnnapp.features.schedule;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

interface ScheduleContract {
    interface View extends BaseFragmentContract.View {
    }
    interface Presenter extends BaseFragmentContract.Presenter { }
}
