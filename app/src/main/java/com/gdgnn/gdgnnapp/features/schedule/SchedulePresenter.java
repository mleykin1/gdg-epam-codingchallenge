package com.gdgnn.gdgnnapp.features.schedule;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class SchedulePresenter extends BasePresenter<ScheduleContract.View> implements ScheduleContract.Presenter {

    private ScheduleContract.View view;

    public SchedulePresenter(ScheduleContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {
    }
}
