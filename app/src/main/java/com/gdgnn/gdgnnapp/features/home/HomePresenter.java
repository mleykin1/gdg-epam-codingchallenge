package com.gdgnn.gdgnnapp.features.home;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {

    private HomeContract.View view;

    public HomePresenter(HomeContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {

    }
}
