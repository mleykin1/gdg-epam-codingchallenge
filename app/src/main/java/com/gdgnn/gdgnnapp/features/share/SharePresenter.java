package com.gdgnn.gdgnnapp.features.share;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class SharePresenter extends BasePresenter<ShareContract.View> implements ShareContract.Presenter {

    private ShareContract.View view;

    public SharePresenter(ShareContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        view.setText("This is share fragment");
    }
}
