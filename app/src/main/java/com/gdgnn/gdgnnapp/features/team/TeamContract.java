package com.gdgnn.gdgnnapp.features.team;

import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragmentContract;

interface TeamContract {
    interface View extends BaseFragmentContract.View {
        void setText(String text);
    }

    interface Presenter extends BaseFragmentContract.Presenter { }
}
