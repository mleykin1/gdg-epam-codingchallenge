package com.gdgnn.gdgnnapp.features.send;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class SendPresenter extends BasePresenter<SendContract.View> implements SendContract.Presenter {

    private SendContract.View view;

    public SendPresenter(SendContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        view.setText("This is send fragment");
    }
}
