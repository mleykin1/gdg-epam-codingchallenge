package com.gdgnn.gdgnnapp.features.photoDetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.gdgnn.gdgnnapp.utils.PicassoHelper;

public class PhotoDetailDialogFragment extends DialogFragment {

    private static final String KEY_URL = "url";

    private String url;

    public static PhotoDetailDialogFragment newInstance(String url) {
        PhotoDetailDialogFragment fragment = new PhotoDetailDialogFragment();
        Bundle args = new Bundle();
        args.putString(KEY_URL, url);
        fragment.setArguments(args);
        fragment.setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        return fragment;
    }

    @Override

    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        url = getArguments().getString(KEY_URL);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Window window = getDialog().getWindow();
        int width = ViewGroup.LayoutParams.MATCH_PARENT;
        int height = ViewGroup.LayoutParams.MATCH_PARENT;
        window.setLayout(width, height);
        ImageView imageView = new ImageView(getContext());
        PicassoHelper.load(getContext(), url, imageView);
        return imageView;
    }
}
