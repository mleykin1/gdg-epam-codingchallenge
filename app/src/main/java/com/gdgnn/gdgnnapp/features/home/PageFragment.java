package com.gdgnn.gdgnnapp.features.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.gdgnn.gdgnnapp.R;

public class PageFragment extends Fragment {
    private TextView mTextView;
    private int mPageNumber;
    public static final String EVENT_DATE = "EVENT_DATE";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments() != null ? getArguments().getInt(EVENT_DATE) : 1;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_fragment, container, false);
        mTextView = view.findViewById(R.id.tv_page_date);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mTextView.setText(mPageNumber == 0 ? "15 November" : "16 November");
    }

    public static PageFragment newInstance(int position){
        PageFragment f = new PageFragment();
        Bundle args = new Bundle();
        args.putInt(EVENT_DATE, position);
        f.setArguments(args);
        return f;
    }
}
