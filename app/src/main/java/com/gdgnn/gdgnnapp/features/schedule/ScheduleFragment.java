package com.gdgnn.gdgnnapp.features.schedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.ViewPager;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;
import com.gdgnn.gdgnnapp.features.adapter.PagerAdapter;

import org.jetbrains.annotations.NotNull;

public class ScheduleFragment extends BaseFragment<ScheduleContract.Presenter> implements ScheduleContract.View {

    private ScheduleContract.Presenter presenter = new SchedulePresenter(this);
    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getActivity() != null) {
            mViewPager = getActivity().findViewById(R.id.view_pager_main);
            mPagerAdapter = new PagerAdapter(getActivity().getSupportFragmentManager(), 1);
            mViewPager.setAdapter(mPagerAdapter);
        }
    }

    @NotNull
    @Override
    protected ScheduleContract.Presenter getPresenter() {
        return presenter;
    }
}