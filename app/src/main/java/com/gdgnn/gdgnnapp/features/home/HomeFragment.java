package com.gdgnn.gdgnnapp.features.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class HomeFragment extends BaseFragment<HomeContract.Presenter> implements HomeContract.View {

    private HomeContract.Presenter presenter = new HomePresenter(this);

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @NotNull
    @Override
    protected HomeContract.Presenter getPresenter() {
        return presenter;
    }
}