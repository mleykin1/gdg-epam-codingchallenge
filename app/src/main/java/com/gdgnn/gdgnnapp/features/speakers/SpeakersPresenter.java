package com.gdgnn.gdgnnapp.features.speakers;

import com.gdgnn.gdgnnapp.base.baseFragment.BasePresenter;

public class SpeakersPresenter extends BasePresenter<SpeakersContract.View> implements SpeakersContract.Presenter {
    private SpeakersContract.View view;

    public SpeakersPresenter(SpeakersContract.View view) {
        super(view);
        this.view = view;
    }

    @Override
    public void onViewCreated() {
        view.setText("This is speakers fragment");
    }
}
