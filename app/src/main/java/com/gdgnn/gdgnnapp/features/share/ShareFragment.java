package com.gdgnn.gdgnnapp.features.share;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.gdgnn.gdgnnapp.R;
import com.gdgnn.gdgnnapp.base.baseFragment.BaseFragment;

import org.jetbrains.annotations.NotNull;

public class ShareFragment extends BaseFragment<ShareContract.Presenter> implements ShareContract.View {

    private ShareContract.Presenter presenter = new SharePresenter(this);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_share, container, false);
    }

    @Override
    public void setText(String text) {
        final TextView textView = getView().findViewById(R.id.text_share);
        textView.setText(text);
    }

    @NotNull
    @Override
    protected ShareContract.Presenter getPresenter() {
        return presenter;
    }
}